import './App.css';
import profile from "./image/a.png";
import email from "./image/email.jpg";
import pass from "./image/pass.png";
import Axios from 'axios';
import React, { useState, useEffect } from 'react';



function App() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [signinStatus, setSigninStatus] = useState("");

  Axios.defaults.withCredentials = true;


  const signIn = () => {
    Axios.post("http://localhost:3001/", {
      email: email,
      password: password,
    }).then((response) => {
      if (response.data.message) {
        setSigninStatus(response.data.message);
      } else {
        setSigninStatus(response.data[0].email);
      }
    });
  };

    useEffect(() => {
      Axios.get("http://localhost:3001/").then((response) => {
        if (response.data.loggedIn === true) {
          setSigninStatus(response.data.user[0].email);
        }
      });
    }, []);

  return (
    <div className="main">
     <div className="sub-main">
       <div>
         <div className="imgs">
           <div className="container-image">
             <img src={profile} alt="profile" className="profile"/>

           </div>


         </div>
         <div>
           <h1>Prijavite se</h1>
           <div>
             <img src={email} alt="email" className="email"/>
             <input type="text" placeholder="Email.." className="name"
             onChange={(e) => {
              setEmail(e.target.value)
            }}
             />
           </div>
           <div className="second-input">
             <img src={pass} alt="pass" className="email"/>
             <input type="password" placeholder="Lozinka.." className="name"
             onChange={(e) => {
              setPassword(e.target.value)
            }}
             />
           </div>
          <div className="login-button">
          <button type="submit" onClick={signIn}> Prijava </button>
          </div>
           
            <p className="link">
              <a href="#">Zaboravili ste lozinku ?</a>
           </p>
 
         </div>
       </div>
       <h1>{signinStatus}</h1>

     </div>
    </div>
  );
}

export default App;