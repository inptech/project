const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const application = express();
const mysql = require('mysql');

const cookieParser = require('cookie-parser');
const session = require('express-session');

application.use (cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST"],
    credentials: true
}));

application.use(cookieParser());
application.use(bodyParser.urlencoded({extended: true}))
application.use(express.json());

application.use(session({
    key: "id",
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 60 * 60 * 24,
    }
}))

const database = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'project'
});

application.post("/", (req,res) => {
    const email = req.body.email;
    const password = req.body.password;


    database.query(
        "SELECT * FROM users WHERE email = ? AND password = ?",
        [email, password],
        (err, result) => {
            if(err) {
                return res.status(400).send(err)
            } 

            if(result.length > 0) {
                req.session.user = result;
                console.log(req.session.user);
                res.send(result);
            } else{
                res.send({ message: "Wrong email/password combination!"});
            }
        } 
    );
});

application.get("/", (req, res) => {
    if (req.session.user) {
        res.send({ loggedIn: true, user: req.session.user });
    } else{
        res.send({ loggedIn: false});
    }
});

application.listen(3001, () => {
    console.log("running server on port 3001!");
});