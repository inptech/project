import './App.css';
import Login from './Pages/Login.js';
import Sidebar from './Components/Sidebar';

function App() {
  return (
    <div>
      <Login />
    </div>
  );
}

export default App;
