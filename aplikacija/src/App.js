import './App.css';
import profile from "./assets/image/a.png";
import email from "./assets/image/email.jpg";
import pass from "./assets/image/pass.png";
import Login from "./pages/Login.js";
import Sidebar from './Components/SidebarData';
import Navbar from './Components/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Reports from './pages/Report';
import Products from './pages/Products';

function App() {
  return (
    <>
    <Router>
    <Navbar/>
    <Switch>
    <Route path='/home' exact component={Home} />
    <Route path='/reports' exact component={Reports} />
    <Route path='/products' exact component={Products} />
    </Switch>
    </Router>
    </>
  );
}

export default App;