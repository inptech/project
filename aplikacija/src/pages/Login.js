import "../styles/Login.css";
import profile from "../assets/image/a.png";
import email from "../assets/image/email.jpg";
import pass from "../assets/image/pass.png";


function Login() {
  return (
    <div className="main">
     <div className="sub-main">
       <div>
         <div className="imgs">
           <div className="container-image">
             <img src={profile} alt="profile" className="profile"/>
           </div>
         </div>
         <div>
           <h1>Prijavite se</h1>
           <div>
             <img src={email} alt="email" className="email"/>
             <input type="text" placeholder="Korisničko ime.." className="name"/>
           </div>
           <div className="second-input">
             <img src={pass} alt="pass" className="email"/>
           <input type="password" placeholder="Lozinka.." className="name"/>
          </div>
          <div className="login-button">
          <button type="submit"> Prijava </button>
          </div>
            <p className="link">
              <a href="#">Zaboravili ste lozinku ?</a>
           </p>
         </div>
       </div>
     </div>
    </div>
  );
}

export default Login;